<?php
require_once "includes/load.php";

switch ($_GET["opcion"]) {
    case 'all':
        $data = find_by_sql('SELECT * FROM carreras', true);
        echo json_encode(array(
            'status' => 1,
            'message' => 'Datos cargados',
            'data' => $data
        ));
        break;
    case 'group_find_by_career':
        $data = find_by_sql("SELECT * FROM grupos WHERE carrera_id={$_REQUEST['carrer_id']}");
        echo json_encode(array(
            'status' => 1,
            'message' => 'Datos cargados',
            'data' => $data
        ));
        break;
    case "students_all":
        $data = find_by_sql('SELECT * FROM alumnos', true);
        echo json_encode(array(
            'status' => 1,
            'message' => 'Datos cargados',
            'data' => $data
        ));
        break;
    case "student_find":
        $data = find_by_id("expedientes", $_REQUEST['alumno_id'], $column = 'alumno_id');
        echo json_encode(array(
            'status' => 1,
            'message' => 'Datos cargados',
            'data' => $data
        ));
        break;
    case 'reason':
        $type = $_REQUEST["type"];
        $sql = "SELECT * FROM motivos WHERE tipo_motivo='{$type}'";
        $data = find_by_sql($sql);
        echo json_encode(array(
            'status' => 1,
            'message' => 'Datos cargados',
            'data' => $data
        ));
        break;
    case 'save':
        $user = current_user();
        $method = $_REQUEST["method"];
        $no_caso = !empty($_REQUEST["no_caso"]) ? $_REQUEST["no_caso"] : NULL;
        $fecha = $_REQUEST["fecha"];
        $programa = $_REQUEST["programa_soli"];
        $cuatri_soli = $_REQUEST["cuatri_soli"];
        $docente_soli = $user["id"]; //Logueado
        $alumno_soli = $_REQUEST["alumno_soli"];
        $programa_soli = $_REQUEST["programa_soli"];
        $matri_soli = $_REQUEST["matri_soli"];
        $grupo_soli = $_REQUEST["grupo_soli"];
        $cuatri_soli = $_REQUEST["cuatri_soli"];
        $motivos = isset($_REQUEST["motivos"]) ? $_REQUEST["motivos"] : NULL;
        $area_id = $_REQUEST["area_id"];
        $folio = null;
        $comentarios = $_REQUEST["comentarios"];

        if ($method == 'insert') {
            $folio = find_by_sql("SELECT `value` FROM folios WHERE tabla='canalizacion' AND serie='SL'", true);
            $folio = intVal($folio[0]['value']) + 1;
        } else {
            $db->query("DELETE FROM canalizacion WHERE no_caso={$no_caso} AND tipo_solicitud='SOLICITUD'");
            $folio = $no_caso;
        }     

        global $db;
        foreach ($motivos as $key => $item) {
            $sql = "INSERT INTO canalizacion (fecha, no_caso, motivo_id, sesion_id, area_id, comentarios, grupo_id, carrera_id, alumno_id, cuatrimestre, docente_id, tipo_solicitud) VALUES('{$fecha}', $folio, $item, NULL, $area_id, '$comentarios', $grupo_soli, $programa_soli, $alumno_soli, '{$cuatri_soli}', {$user["id"]}, 'SOLICITUD');";
            $db->query($sql);
        }

        $result =  ($db->affected_rows() === 1) ? true : false;
        if ($result) {

            //Actualizamos el folio consecutivo de la solicitud
            if (empty($no_caso) && $method == "insert") {
                $update = "UPDATE folios SET value = value + 1 WHERE tabla='canalizacion' AND  serie = 'SL'";
                $db->query($update);
            }
            echo json_encode(array(
                'status' => 1,
                'message' => 'Solicitud guardada',
                'data' => $result
            ));
        } else {
            echo json_encode(array(
                'status' => 0,
                'message' => 'Error al guardar la solicitud',
                'data' => $result
            ));
        }
        break;
    case "destroy":
        $id = $_REQUEST["id"];
        $sql = "DELETE FROM canalizacion WHERE no_caso={$id} AND tipo_solicitud='SOLICITUD'";
        $result = $db->query($sql);
        if ($result) {
            echo json_encode(array(
                'status' => 1,
                'message' => 'Solicitud eliminada correctamente',
                'data' => $result
            ));
        } else {
            echo json_encode(array(
                'status' => 0,
                'message' => 'Error al eliminar la solicitud',
                'data' => $result
            ));
        }
        break;
    default:

        break;
}
