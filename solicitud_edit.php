<?php
$page_title = 'Solicitud';

require_once('includes/load.php');
if (!$session->isUserLoggedIn(true)) {
    redirect('index.php', false);
}

$user = current_user();
$docente = "{$user['nombre']} {$user['apellido_paterno']} {$user['apellido_materno']}";

//Consulta para obtener los motivos de tipo SOLICITUD
$sql = "SELECT * FROM motivos WHERE tipo_motivo='SOLICITUD'";
$reason = find_by_sql($sql, true);

//Consulta para obtener las areas
$areas = find_by_sql("SELECT * FROM areas", true);

//Obtener los programas
$programas = find_by_sql("SELECT * FROM carreras", true);

//Consulta la solicitud por no_caso
$sql = "SELECT * FROM canalizacion WHERE tipo_solicitud='SOLICITUD' AND no_caso=" . $_REQUEST["no_caso"];
$request = find_by_sql($sql, true);

//Obtener el docente
$docente_solicitud = find_by_id("docentes", $request[0]['docente_id'], "id");

//Alumnos 
$alumnos = find_by_sql("SELECT * FROM alumnos", true);

//Matricula
$matricula = find_by_sql("SELECT * FROM expedientes WHERE alumno_id =" . $request[0]["alumno_id"] . " LIMIT 1", true);

//Grupos
$grupos = find_by_sql("SELECT * FROM grupos WHERE carrera_id =" . $request[0]["carrera_id"], true);


?>
<?php include_once('template/header.php'); ?>
<section class="section-content">
    <article class="article-content">       
        <form id="form-canalizacion" method="POST" class="form-canalizacion">
            <div class="accion">
                <h3 class="subtitle">Solicitud de Canalización</h3>
                <ul class="elements">
                    <li class="list-elements">
                        <label for="fecha">Fecha de solicitud:</label>
                        <input type="date" name="fecha" id="fecha" value="<?= $request[0]['fecha'] ?>" required>
                    </li>
                    <li class="list-elements">
                    </li>
                    <li class="list-elements">
                    </li>
                    <li class="list-elements">
                        <label for="programa_soli">Programa Educativo:</label>
                        <select name="programa_soli" id="programa_soli" required>
                            <option value='0'>Selecciona una opción</option>
                            <?php 
                            foreach ($programas as $key => $item) {
                                ?>
                                <option <?=($request[0]["carrera_id"] == $item["id"]) ? 'selected': ''?> value="<?=$item["id"]?>"><?=$item["nombre"]?></option>
                                <?php
                            }
                            ?>
                        </select>
                    </li>
                    <li class="list-elements">
                        <label for="cuatri_soli">Cuatrimestre:</label>
                        <select name="cuatri_soli" id="cuatri_soli" required>
                            <option  <?=($request[0]["cuatrimestre"] == 'primero')? 'selected': ''?> value="primero">Primero</option>
                            <option <?=($request[0]["cuatrimestre"] == 'segundo')? 'selected': ''?> value="segundo">Segundo</option>
                            <option <?=($request[0]["cuatrimestre"] == 'tercero')? 'selected': ''?> value="tercero">Tercero</option>
                            <option <?=($request[0]["cuatrimestre"] == 'cuarto')? 'selected': ''?> value="cuarto">Cuarto</option>
                            <option <?=($request[0]["cuatrimestre"] == 'quinto')? 'selected': ''?> value="quinto">Quinto</option>
                            <option <?=($request[0]["cuatrimestre"] == 'sexto')? 'selected': ''?> value="sexto">Sexto</option>
                            <option <?=($request[0]["cuatrimestre"] == 'septimo')? 'selected': ''?> value="septimo">Séptimo</option>
                            <option <?=($request[0]["cuatrimestre"] == 'octavo')? 'selected': ''?> value="octavo">Octavo</option>
                            <option <?=($request[0]["cuatrimestre"] == 'noveno')? 'selected': ''?> value="noveno">Noveno</option>
                            <option <?=($request[0]["cuatrimestre"] == 'decimo')? 'selected': ''?> value="decimo">Decimo</option>
                        </select>
                    </li>
                    <li class="list-elements">
                        <label for="docente_soli">Nombre de quién solicita:</label>
                        <input type="text" name="docente_soli" id="docente_soli" value="<?= $docente_solicitud["nombre"] . " ". $docente_solicitud["apellido_paterno"] . " " . $docente_solicitud["apellido_materno"] ?>" readonly>
                    </li>
                    <li class="list-elements">
                        <label for="alumno_soli">Nombre del alumno(a):</label>
                        <select name="alumno_soli" id="alumno_soli">
                            <option value="0">Selecciona una opción</option>
                            <?php 
                            foreach ($alumnos as $key => $item) {
                                ?>
                                <option <?=($request[0]["alumno_id"] == $item["id"])? 'selected': ''?> value="<?=$item["id"]?>"><?=$item["nombre"] . " " . $item["apellido_paterno"] . " " . $item["apellido_materno"] ?></option>
                                <?php
                            }
                            ?>                            
                        </select>
                    </li>
                    <li class="list-elements">
                        <label for="matri_soli">Matricula:</label>
                        <input type="number" name="matri_soli" id="matri_soli" value="<?=$matricula[0]["matricula"]?>" readonly required>
                    </li>
                    <li class="list-elements">
                        <label for="grupo_soli">Grupo:</label>
                        <select name="grupo_soli" id="grupo_soli" required>
                            <option value="0">Selecciona una opción</option>
                            <?php
                            foreach ($grupos as $key => $item) {
                                ?>
                                <option <?=($request[0]["grupo_id"] == $item["id"]) ? 'selected': ''?> value="<?=$item["id"]?>"><?=$item["nomenclatura"]?></option>
                                <?php
                            }
                            ?>
                        </select>
                    </li>
                </ul>
            </div>
            <div class="accion serp">
                <h2 class="subtitle">Motivos de Solicitud</h2>
                <ul class="elements">
                    <?php
                    foreach ($reason as $key => $item) {
                        $finded = false;
                        foreach ($request as $key => $req) {
                            
                            if ($req["motivo_id"] == $item["id"]) {
                                $finded = true;
                            }
                        }

                    ?>
                        <li class="list-elements">
                            <label for="motivos"><?= $item['motivo'] ?></label>
                            <input class="motivos" type="checkbox" name="motivos[]" id="motivos" value="<?= $item['id'] ?>" <?=($finded) ? 'checked': ''?>>
                        </li>
                    <?php
                    }
                    ?>
                </ul>
            </div>
            <div class="accion serp">
                <h2 class="subtitle">Área a canalizar</h2>
                <ul class="elements">
                    <li class="list-elements">
                        <select name="area_id" id="area_id">
                            <option value="">Selecciona una opción</option>
                            <?php
                            foreach ($areas as $key => $item) {
                            ?>
                                <option <?=($request[0]["area_id"] == $item["id"])? 'selected': ''?> value="<?= $item["id"] ?>"><?= $item["area"] ?></option>
                            <?php
                            }
                            ?>
                        </select>
                    </li>
                </ul>
            </div>
            <div class="accion serp">
                <ul class="tabs">
                    <li class="list-tabs">
                        <a href="#tab1">Comentarios del solicitante:</a>
                    </li>
                </ul>
                <div class="secciones-tab">
                    <article class="article-tab" id="tab1">
                        <textarea name="comentarios" id="comentarios" placeholder="Escribe un comentario"><?=$request[0]["comentarios"]?></textarea>
                    </article>
                </div>
            </div>
            <div class="accion serp">
                <ul class="buttons">
                    <li class="list-buttons">
                        <button id="btn-return" name="btn-return" class="btn nuevo" type="button">Regresar</button>                        
                    </li>                    
                    <li class="list-buttons">
                        <button class="btn guardar" id="save" name="save" value="save" type="submit">Guardar</button>
                    </li>
                </ul>
            </div>
            <input type="hidden" name="no_caso" id="no_caso" value="<?=$request[0]["no_caso"]?>">
            <input type="hidden" name="method" id="method" value="update">
        </form>
    </article>
</section>

<script>
    $(function() {

        $('#btn-return').on('click', function (e) {
            window.location.href = "solicitud_index.php";
        });  

        $('#programa_soli').on('change', function(e) {
            let _id = $(this).val();
            $('#grupo_soli').empty();
            $('#grupo_soli').append(`<option value='0'>Selecciona una opción</option>`);

            getGroupsByCarrer(_id).then(res => {                
                if (res.status) {
                    res.data.forEach(element => {
                        $('#grupo_soli').append(`<option ${(element.id == <?=$request[0]["carrera_id"]?>) ? 'selected': ''} value='${element.id}'>${element.nomenclatura}</option>`);
                    });
                } else {
                    $('#grupo_soli').append(`<option value='0'>Selecciona una opción</option>`);
                }
            }).catch(console.log);
        }); 
      

        //Cambiando opcion alumno
        $('#alumno_soli').on('change', function(e) {
            let _id = $(this).val();
            getEnrollment(_id).then(res => {
                $('#matri_soli').val(res.data.matricula);
            }).catch(console.log);

        });

        $('#form-canalizacion').submit(function(e) {
            e.preventDefault();
            let form = $('#form-canalizacion').serialize();
            let _programa_soli = $('#programa_soli').val();
            let _alumno_soli = $('#alumno_soli').val();
            let _grupo_soli = $('#grupo_soli').val();
            let _area_id = $('#area_id').val();

            if (_programa_soli == 0 || _programa_soli == null) {
                alert('Debe seleccionar un programa educativo');
                return false;
            }

            if (_alumno_soli == 0 || _alumno_soli == null) {
                alert('Debe seleccionar un alumno');
                return false;
            }

            if (_grupo_soli == 0 || _grupo_soli == null) {
                alert('Debe seleccionar un grupo');
                return false;
            }


            if (_area_id == 0 || _area_id == null) {
                alert('Debe seleccionar un área a canalizar');
                return false;
            }

            //if ($('input:checkbox', this).length == $('input:checked', this).length) {
            if ($('input:checkbox', this).length > 0) {
                store(form).then(res => {
                    if (res.status) {
                        alert('Solicitud guardada correctamente');
                        window.location.href = "solicitud_index.php";
                    } else {
                        alert(`${res.message}, intente, más tarde!`);
                    }
                }).catch(console.log);

            } else {
                alert('Debe seleccionar al menos un motivo');
                return false;
            }
        });
    });

    const getPrograms = function() {
        return new Promise((resolve, reject) => {
            $.ajax({
                url: 'solicitud_ajax.php?opcion=all',
                method: 'get',
                dataType: "json",
            }).done(resolve).fail(reject);
        });
    }

    const getStudents = function() {
        return new Promise((resolve, reject) => {
            $.ajax({
                url: 'solicitud_ajax.php?opcion=students_all',
                method: 'get',
                dataType: "json",
            }).done(resolve).fail(reject);
        });
    }

    const getEnrollment = function(id) {
        return new Promise((resolve, reject) => {
            $.ajax({
                url: 'solicitud_ajax.php?opcion=student_find',
                method: 'get',
                dataType: "json",
                data: {
                    alumno_id: id
                }
            }).done(resolve).fail(reject);
        });
    }

    const getGroupsByCarrer = function(carrer_id) {
        return new Promise((resolve, reject) => {
            $.ajax({
                url: 'solicitud_ajax.php?opcion=group_find_by_career',
                method: 'get',
                dataType: "json",
                data: {
                    carrer_id: carrer_id
                }
            }).done(resolve).fail(reject);
        });
    }

    const store = function(data) {
        return new Promise((resolve, reject) => {
            $.ajax({
                url: 'solicitud_ajax.php?opcion=save',
                method: 'post',
                dataType: "json",
                data: data
            }).done(resolve).fail(reject);
        });
    }
</script>
<?php include_once('template/footer.php'); ?>