<?php
$page_title = 'Plan acción';

require_once('includes/load.php');
if (!$session->isUserLoggedIn(true)) {
    redirect('index.php', false);
}

$user = current_user();
$docente = "{$user['nombre']} {$user['apellido_paterno']} {$user['apellido_materno']}";

//Consulta para obtener los motivos de tipo SOLICITUD
$sql = "SELECT * FROM motivos WHERE tipo_motivo='SOLICITUD'";
$reason = find_by_sql($sql, true);

//Consulta para obtener las areas
$sql = "SELECT * FROM areas";
$areas = find_by_sql($sql, true);

$sql = "SELECT c.*, ts.descripcion as motivo, a.area, CONCAT(g.nomenclatura, ' ', g.anio) AS grupo,
c2.nombre AS carrera, CONCAT(a2.nombre, ' ', a2.apellido_paterno, ' ', a2.apellido_materno) AS alumno,
CONCAT(d.nombre, ' ', d.apellido_paterno, ' ', d.apellido_materno) AS docente
FROM canalizacion c 
INNER JOIN tipo_sesion ts ON c.motivo_id = ts.id
INNER JOIN areas a ON c.area_id = a.id
INNER JOIN grupos g ON c.grupo_id = g.id
INNER JOIN carreras c2 ON c.carrera_id = c2.id
INNER JOIN alumnos a2 ON c.alumno_id = a2.id
INNER JOIN docentes d ON c.docente_id = d.id 
WHERE c.docente_id = {$user["id"]} AND c.tipo_solicitud='PLAN'
GROUP BY c.no_caso ORDER BY c.id DESC";
$requests = find_by_sql($sql, true);

?>
<?php include_once('template/header.php'); ?>
<section class="section-content">
    <article class="article-content">
        <div class="form-canalizacion">
            <div class="accion serp">
                <h3 class="subtitle">Lista de planes de acción tutoríal
                    <ul class="buttons">
                        <li class="list-buttons">
                            <button id="btn-new" name="btn-new" class="btn nuevo" type="reset">Nuevo</button>                            
                        </li>
                    </ul>
                </h3>
            </div>
            <table style="width: 100%;">
                <thead>
                    <tr>                        
                        <th># Solicitud</th>
                        <th>Fecha</th>
                        <th>Alumno</th>   
                        <th>Grupo</th>                     
                        <th>Programa Educativo</th>
                        <th>Cuatrimestre</th>
                        <th>Area</th>
                        <th>Motivo</th>
                        <!-- <th>Comentarios</th> -->
                        <th>Acciones</th>
                    </tr>
                </thead>
                <tbody>
                <?php 
                   foreach ($requests as $key => $item) {
                       ?>
                       <tr>                           
                           <td><?=$item["no_caso"]?></td>
                           <td><?=$item["fecha"]?></td>
                           <td><?=$item["alumno"]?></td>
                           <td><?=$item["grupo"]?></td>
                           <td><?=$item["carrera"]?></td>
                           <td><?=$item["cuatrimestre"]?></td>
                           <td><?=$item["area"]?></td>
                           <td><?=$item["motivo"]?></td>
                           <!-- <td><?=$item["comentarios"]?></td> -->
                           <td style="width: 110px;">
                               <a data-id="<?=$item["no_caso"]?>" href="plan_edit.php?no_caso=<?=$item["no_caso"]?>" class="btn guardar btn-edit" title="Editar" style="color: #28a745;"><i class="fa fa-2x fa-edit"></i></a>
                               <a data-id="<?=$item["no_caso"]?>" href="plan_print.php?no_caso=<?=$item["no_caso"]?>" class="btn guardar btn-edit" title="Editar" style="color: #28a745;"><i class="fa fa-2x fa-print"></i></a>
                               <a data-id="<?=$item["no_caso"]?>" href="javascript:void(0)" class="btn guardar btn-delete" title="Eliminar" style="color: #dc3545;"><i class="fa fa-trash"></i></a>
                           </td>
                       </tr>
                       <?php
                   }
                   ?>
                </tbody>
            </table>
        </div>        
    </article>
</section>

<script>
    $(function() {
        $('#btn-new').on('click', function (e) {
            window.location.href = "plan_new.php";
        });

        $('.btn-delete').on('click', function (e) {
            let _id = $(this).data('id');            
            if (confirm('Desea eliminar la solicitud ?')) {
                destroy(_id).then(res => {                        
                    if (res.status) {
                        location.reload();
                    }                    
                }).catch(console.log);
            }
        });
    });

    const destroy = function (no_caso) {
        return new Promise((resolve, reject) => {
            $.ajax({
                url: 'plan_ajax.php?opcion=destroy',
                method: 'get',
                dataType: "json",
                data: {id: no_caso}
            }).done(resolve).fail(reject);
        });   
    }
</script>
<?php include_once('template/footer.php'); ?>