<?php
ob_start();
require_once('includes/load.php');
if ($session->isUserLoggedIn(true)) {
    redirect('home', false);
}
?>
<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <title>Acción Tutorias</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css">
    <link rel="stylesheet" href="<?= BASE_URL ?>/assets/css/login.css">
    <script src="<?= BASE_URL ?>/assets/js/jquery-3.5.1.js"></script>
</head>

<body>

    <main id="main">
        <section id="section-container" class="section-container">
            <article class="article_container">
                <div class="container-title">
                    <h2 class="title">Acción Tutorias</h2>
                    <img src="<?=BASE_URL?>/assets/img/logo.png" alt="Logo-Upfim">
                </div>
                <form action="auth.php" method="POST" class="formulario_login" id="formulario_login">
                    <ul>
                        <li>
                            <label for="matricula">
                                <i class="fas fa-user"></i>
                                <input type="number" class="matricula" name="matricula" id="matricula" placeholder="Matricula" required>
                            </label>
                        </li>
                        <li>
                            <label for="pass">
                                <i class="fas fa-lock"></i>
                                <input type="password" class="pass" id="password" name="password" placeholder="Contraseña" required>
                            </label>
                        </li>
                        <li>
                            <?php echo display_msg($msg); ?>
                        </li>
                        <li>
                            <input type="submit" class="btn_ingresar" id="btn_ingresar" value="Ingresar">
                        </li>
                    </ul>
                </form>
            </article>
            <article class="article_vector">
                <img src="<?=BASE_URL?>/assets/img/vector.png" alt="" class="img_vector">
            </article>
        </section>
    </main>
</body>
</html>