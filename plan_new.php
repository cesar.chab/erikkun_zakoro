<?php
$page_title = 'Plan acción';

require_once('includes/load.php');
if (!$session->isUserLoggedIn(true)) {
    redirect('index.php', false);
}

$user = current_user();
$docente = "{$user['nombre']} {$user['apellido_paterno']} {$user['apellido_materno']}";

//Programas / carreras
$programas = find_by_sql("SELECT * FROM carreras", true);

$periods = find_by_sql("SELECT * FROM periodo_cuatrimestral", true);

//Consulta para obtener los motivos de tipo SOLICITUD
$sql = "SELECT * FROM motivos WHERE tipo_motivo='PLAN'";
$reason = find_by_sql($sql, true);

//Consulta para obtener las areas
$sql = "SELECT * FROM areas";
$areas = find_by_sql($sql, true);

//Obtener alumnos
$alumnos = find_by_sql("SELECT * FROM alumnos", true);

//Obtener los tipos de sesiones
$sessions = find_by_sql("SELECT * FROM tipo_sesion", true);



?>
<?php include_once('template/header.php'); ?>
<section class="section-content">
    <article class="article-content">
        <form id="form-canalizacion" method="POST" class="form-canalizacion">
            <div class="accion">
                <h2 class="subtitle">Plan Acción Tutoríal</h2>
                <ul class="elements">
                    <li class="list-elements">
                        <label for="fehca">Fecha de registro:</label>
                        <input type="date" name="fecha" id="fecha" value="<?= date('Y-m-d') ?>" required>
                    </li>
                    <li class="list-elements">
                    </li>
                    <li class="list-elements">
                    </li>
                    <li class="list-elements">
                        <label for="programa_accion">Programa Educativo:</label>
                        <select name="programa_soli" id="programa_soli" required>
                            <option value='0'>Selecciona una opción</option>
                            <?php
                            foreach ($programas as $key => $item) {
                            ?>
                                <option value="<?= $item["id"] ?>"><?= $item["nombre"] ?></option>
                            <?php
                            }
                            ?>
                        </select>
                    </li>
                    <li class="list-elements">
                        <label for="cuatri_accion">Cuatrimestre:</label>
                        <select name="cuatri_accion" id="cuatri_accion">
                            <option value="primero">Primero</option>
                            <option value="segundo">Segundo</option>
                            <option value="tercero">Tercero</option>
                            <option value="cuarto">Cuarto</option>
                            <option value="quinto">Quinto</option>
                            <option value="sexto">Sexto</option>
                            <option value="septimo">Séptimo</option>
                            <option value="octavo">Octavo</option>
                            <option value="noveno">Noveno</option>
                            <option value="decimo">Decimo</option>
                        </select>
                    </li>
                    <li class="list-elements">
                        <label for="periodo">Periodo cuatrimestral:</label>
                        <select name="periodo" id="periodo">
                            <option value='0'>Selecciona una opción</option>
                            <?php
                            foreach ($periods as $key => $item) {
                            ?>
                                <option value="<?= $item["id"] ?>"><?= $item["descripcion"] ?></option>
                            <?php
                            }
                            ?>
                        </select>
                    <li class="list-elements">
                        <label for="jefe_accion">Nombre del docente:</label>
                        <!-- <input type="text" name="jefe_accion" id="jefe_accion"> -->
                        <input type="text" name="docente_soli" id="docente_soli" value="<?= $docente ?>" readonly>
                    </li>
                    <li class="list-elements">
                        <label for="jefe_grupo">Jefe de grupo:</label>
                        <select name="alumno_soli" id="alumno_soli">
                            <option value="0">Selecciona una opción</option>
                            <?php
                            foreach ($alumnos as $key => $item) {
                            ?>
                                <option value="<?= $item["id"] ?>"><?= $item["nombre"] . " " . $item["apellido_paterno"] . " " . $item["apellido_materno"] ?></option>
                            <?php
                            }
                            ?>
                        </select>
                    </li>
                    <li class="list-elements">
                        <label for="matri_soli">Matricula:</label>
                        <input type="number" name="matri_soli" id="matri_soli" readonly required>
                    </li>
                    <li class="list-elements">
                        <label for="grupo_accion">Grupo:</label>
                        <select name="grupo_soli" id="grupo_soli" required>
                            <option data-mens="0" data-women="0" value="0">Selecciona una opción</option>
                        </select>
                    </li>
                    <li class="list-elements">
                        <label for="mujeres_accion">Total de mujeres:</label>
                        <input type="number" name="mujeres_accion" id="mujeres_accion">
                    </li>
                    <li class="list-elements">
                        <label for="hombres_accion">Total de hombres:</label>
                        <input type="number" name="hombres_accion" id="hombres_accion">
                    </li>
                    <li class="list-elements">
                        <label for="parcial">Parcial:</label>
                        <select name="parcial" id="parcial">
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                        </select>                        
                    </li>

                </ul>
            </div>
            <div class="accion serp">
                <h2 class="subtitle">Tipo de canalización</h2>
                <ul class="elements">
                    <li class="list-elements">
                        <label for="asesoria_accion"></label>
                        <select name="asesoria_accion" id="asesoria_accion">
                            <option value='0'>Selecciona una opción</option>
                            <?php
                            foreach ($sessions as $key => $item) {
                            ?>
                                <option value="<?= $item["id"] ?>"><?= $item["descripcion"] ?></option>
                            <?php
                            }
                            ?>
                        </select>
                    </li>
                </ul>
            </div>
            <div class="accion serp">
                <h2 class="subtitle">Área a canalizar</h2>
                <ul class="elements">
                    <li class="list-elements">
                        <select name="area_id" id="area_id">
                            <option value="">Selecciona una opción</option>
                            <?php
                            foreach ($areas as $key => $item) {
                            ?>
                                <option value="<?= $item["id"] ?>"><?= $item["area"] ?></option>
                            <?php
                            }
                            ?>
                        </select>
                    </li>
                </ul>
            </div>
            <div class="accion serp">
                <ul class="tabs">
                    <li class="list-tabs">
                        <a href="#tab1">Comentarios del tutor</a>
                    </li>
                    <li class="list-tabs">
                        <a href="#tab2">Temática a abordar</a>
                    </li>
                    <li class="list-tabs">
                        <a href="#tab3">Resultados Esperados</a>
                    </li>
                    <li class="list-tabs">
                        <a href="#tab4">Resultados Obtenidos</a>
                    </li>
                </ul>
                <div class="secciones-tab">
                    <article class="article-tab" id="tab1">
                        <textarea name="comentarios" id="comentarios" placeholder="Comentarios del  Tutor"></textarea>
                    </article>
                    <article class="article-tab" id="tab2">
                        <textarea name="tematica" id="tematica" placeholder="Temática a aborda"></textarea>
                    </article>
                    <article class="article-tab" id="tab3">
                        <textarea name="resultados_esperados" id="resultados_esperados" placeholder="Resultados Esperados"></textarea>
                    </article>
                    <article class="article-tab" id="tab4">
                        <textarea name="resultados_obtenidos" id="resultados_obtenidos" placeholder="Resultados Obtenidos"></textarea>
                    </article>
                </div>
            </div>

            </div>

            <div class="accion serp">
                <ul class="buttons">
                    <li class="list-buttons">
                        <button id="btn-return" name="btn-return" class="btn nuevo" type="button">Regresar</button>
                    </li>
                    <li class="list-buttons">
                        <button class="btn guardar" type="submit">Guardar</button>
                    </li>
                </ul>
            </div>
            <input type="hidden" name="method" id="method" value="insert">
            <input type="hidden" name="no_caso" id="no_caso">
        </form>
    </article>
</section>

<script>
    $(function() {
        $('#btn-return').on('click', function(e) {
            window.location.href = "plan_index.php";
        });

        $('#programa_soli').on('change', function(e) {
            let _id = $(this).val();
            let _cuatrimestre_id = $('#periodo').val();
            $('#grupo_soli').empty();
            $('#grupo_soli').append(`<option data-mens='0' data-women='0' value='0'>Selecciona una opción</option>`);

            getGroupsByCarrer(_id, _cuatrimestre_id).then(res => {
                console.log(res)
                if (res.status) {
                    res.data.forEach(element => {
                        $('#grupo_soli').append(`<option data-mens='${element.total_hombres}' data-women='${element.total_mujeres}' value='${element.id}'>${element.nomenclatura}</option>`);
                    });
                } else {
                    $('#grupo_soli').append(`<option data-mens='0' data-women='0' value='0'>Selecciona una opción</option>`);
                }
            }).catch(console.log);
        });

        $('#periodo').on('change', function(e) {
            $('#programa_soli').change();
        });

        $('#grupo_soli').on('change', function(e) {

            let _total_mens = $(this).find(':selected').data('mens');
            let _total_women = $(this).find(':selected').data('women');
            $('#mujeres_accion').val(_total_women);
            $('#hombres_accion').val(_total_mens);
        });

        //Cambiando opcion alumno
        $('#alumno_soli').on('change', function(e) {
            let _id = $(this).val();
            getEnrollment(_id).then(res => {
                $('#matri_soli').val(res.data.matricula);
            }).catch(console.log);

        });

        $('#form-canalizacion').submit(function(e) {
            e.preventDefault();
            let form = $('#form-canalizacion').serialize();
            let _programa_soli = $('#programa_soli').val();
            let _alumno_soli = $('#alumno_soli').val();
            let _grupo_soli = $('#grupo_soli').val();
            let _area_id = $('#area_id').val();
            let _asesoria_accion = $('#asesoria_accion').val();


            if (_programa_soli == 0 || _programa_soli == null) {
                alert('Debe seleccionar un programa educativo');
                return false;
            }

            if (_alumno_soli == 0 || _alumno_soli == null) {
                alert('Debe seleccionar un alumno');
                return false;
            }

            if (_grupo_soli == 0 || _grupo_soli == null) {
                alert('Debe seleccionar un grupo');
                return false;
            }

            if (_asesoria_accion == 0 || _asesoria_accion == null) {
                alert('Debe seleccionar un tipo de canalización');
                return false;
            }


            if (_area_id == 0 || _area_id == null) {
                alert('Debe seleccionar un área a canalizar');
                return false;
            }

            store(form).then(res => {
                if (res.status) {
                    alert('Plan guardada correctamente');
                    window.location.href = "plan_index.php";
                } else {
                    alert(`${res.message}, intente, más tarde!`);
                }
            }).catch(console.log);

        });


    });

    const getPrograms = function() {
        return new Promise((resolve, reject) => {
            $.ajax({
                url: 'plan_ajax.php?opcion=all',
                method: 'get',
                dataType: "json",
            }).done(resolve).fail(reject);
        });
    }

    const getGroupsByCarrer = function(carrer_id, cuatrimestre_id) {
        return new Promise((resolve, reject) => {
            $.ajax({
                url: 'plan_ajax.php?opcion=group_find_by_career',
                method: 'get',
                dataType: "json",
                data: {
                    carrer_id: carrer_id,
                    cuatrimestre_id: cuatrimestre_id
                }
            }).done(resolve).fail(reject);
        });
    }

    const store = function(data) {
        return new Promise((resolve, reject) => {
            $.ajax({
                url: 'plan_ajax.php?opcion=save',
                method: 'post',
                dataType: "json",
                data: data
            }).done(resolve).fail(reject);
        });
    }

    const getEnrollment = function(id) {
        return new Promise((resolve, reject) => {
            $.ajax({
                url: 'plan_ajax.php?opcion=student_find',
                method: 'get',
                dataType: "json",
                data: {
                    alumno_id: id
                }
            }).done(resolve).fail(reject);
        });
    }
</script>
<?php include_once('template/footer.php'); ?>