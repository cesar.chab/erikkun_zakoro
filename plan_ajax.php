<?php
require_once "includes/load.php";

switch ($_GET["opcion"]) {
    case 'all':
        $data = find_by_sql('SELECT * FROM carreras', true);
        echo json_encode(array(
            'status' => 1,
            'message' => 'Datos cargados',
            'data' => $data
        ));
        break;
    case 'group_find_by_career':
        $data = find_by_sql("SELECT * FROM grupos WHERE carrera_id={$_REQUEST['carrer_id']} AND periodo_cuatrimestral_id={$_REQUEST['cuatrimestre_id']}");
        echo json_encode(array(
            'status' => 1,
            'message' => 'Datos cargados',
            'data' => $data
        ));
        break;
    case "students_all":
        $data = find_by_sql('SELECT * FROM alumnos', true);
        echo json_encode(array(
            'status' => 1,
            'message' => 'Datos cargados',
            'data' => $data
        ));
        break;
    case "student_find":
        $data = find_by_id("expedientes", $_REQUEST['alumno_id'], $column = 'alumno_id');
        echo json_encode(array(
            'status' => 1,
            'message' => 'Datos cargados',
            'data' => $data
        ));
        break;
    case 'reason':
        $type = $_REQUEST["type"];
        $sql = "SELECT * FROM motivos WHERE tipo_motivo='{$type}'";
        $data = find_by_sql($sql);
        echo json_encode(array(
            'status' => 1,
            'message' => 'Datos cargados',
            'data' => $data
        ));
        break;
    case 'save':
        $user = current_user();
        $method = $_REQUEST["method"];
        $no_caso = !empty($_REQUEST["no_caso"]) ? $_REQUEST["no_caso"] : NULL;
        $fecha = $_REQUEST["fecha"];
        $programa_soli = $_REQUEST["programa_soli"];
        $cuatri_accion = $_REQUEST["cuatri_accion"];
        $periodo = $_REQUEST["periodo"];
        $docente_soli = $user["id"]; //Logueado        
        $alumno_soli = $_REQUEST["alumno_soli"];
        $matri_soli = $_REQUEST["matri_soli"];
        $grupo_soli = $_REQUEST["grupo_soli"];
        $asesoria_accion = $_REQUEST["asesoria_accion"];
        $area_id = $_REQUEST["area_id"];
        $comentarios = $_REQUEST["comentarios"];
        $tematica = $_REQUEST["tematica"];
        $resultados_esperados = $_REQUEST["resultados_esperados"];
        $resultados_obtenidos = $_REQUEST["resultados_obtenidos"];
        $mujeres_accion = $_REQUEST["mujeres_accion"];
        $hombres_accion = $_REQUEST["hombres_accion"];
        $parcial = $_REQUEST["parcial"];

        $folio = null;


        if ($method == 'insert') {
            $folio = find_by_sql("SELECT `value` FROM folios WHERE tabla='planeacion' AND serie='PL'", true);
            $folio = intVal($folio[0]['value']) + 1;
        } else {
            $db->query("DELETE s FROM sesion s INNER JOIN canalizacion c ON s.no_sesion = c.no_caso AND c.tipo_solicitud ='PLAN' WHERE no_sesion = {$no_caso}");

            $db->query("DELETE FROM canalizacion WHERE no_caso={$no_caso} AND tipo_solicitud='PLAN'");
            
            $folio = $no_caso;
        }

        global $db;
        $sql = "INSERT INTO canalizacion (fecha, no_caso, motivo_id, sesion_id, area_id, comentarios, grupo_id, carrera_id, alumno_id, cuatrimestre, docente_id, tipo_solicitud, periodo_catrimestral) 
            VALUES('{$fecha}', $folio, $asesoria_accion, NULL, $area_id, '$comentarios', $grupo_soli, $programa_soli, $alumno_soli, '{$cuatri_accion}', {$user["id"]}, 'PLAN', {$periodo});";
        $db->query($sql);

        //Insertar en sesion
        $sql = "INSERT INTO sesion (no_sesion, tematica, objetivo, fecha, resultados_esperados, resultados_obtenidos, parcial, tipo_sesion_id, matricula, planeacion_tutoria_id, comentarios) 
            VALUES({$folio}, '{$tematica}', '', '{$fecha}', '{$resultados_esperados}', '{$resultados_obtenidos}', {$parcial}, $asesoria_accion, '{$matri_soli}', NULL, '{$comentarios}');";
        $db->query($sql);

        $result =  ($db->affected_rows() === 1) ? true : false;
        if ($result) {

            //Actualizamos el total de hombre y mujeres por grupo
            $updGrupo = $db->query("UPDATE grupos SET total_mujeres={$mujeres_accion}, total_hombres={$hombres_accion} WHERE id = {$grupo_soli}");


            //Actualizamos el folio consecutivo de la solicitud
            if (empty($no_caso) && $method == "insert") {                
                $db->query("UPDATE folios SET value = value + 1 WHERE tabla='planeacion' AND  serie = 'PL'");
            }
            echo json_encode(array(
                'status' => 1,
                'message' => 'Plan acción tutoríal guardada',
                'data' => $result
            ));
        } else {
            echo json_encode(array(
                'status' => 0,
                'message' => 'Error al guardar',
                'data' => $result
            ));
        }
        break;
    case "destroy":
        $id = $_REQUEST["id"];
        $db->query("DELETE s FROM sesion s INNER JOIN canalizacion c ON s.no_sesion = c.no_caso AND c.tipo_solicitud ='PLAN' WHERE no_sesion = {$id}");
        $result = $db->query("DELETE FROM canalizacion WHERE no_caso={$id} AND tipo_solicitud='PLAN'");
        if ($result) {
            echo json_encode(array(
                'status' => 1,
                'message' => 'Solicitud eliminada correctamente',
                'data' => $result
            ));
        } else {
            echo json_encode(array(
                'status' => 0,
                'message' => 'Error al eliminar la solicitud',
                'data' => $result
            ));
        }
        break;
    default:

        break;
}
