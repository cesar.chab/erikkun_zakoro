<?php
require_once('includes/load.php');
if (!$session->isUserLoggedIn(true)) {
    redirect('index.php', false);
}

$user = current_user();
$docente = "{$user['nombre']} {$user['apellido_paterno']} {$user['apellido_materno']}";

//Consulta para obtener los motivos de tipo SOLICITUD
$sql = "SELECT * FROM motivos WHERE tipo_motivo='SOLICITUD'";
$reason = find_by_sql($sql, true);

//Consulta para obtener las areas
$sql = "SELECT * FROM areas";
$areas = find_by_sql($sql, true);

$sql = "SELECT c.*, m.motivo, a.area, CONCAT(g.nomenclatura, ' ', g.anio) AS grupo,
c2.nombre AS carrera, CONCAT(a2.nombre, ' ', a2.apellido_paterno, ' ', a2.apellido_materno) AS alumno,
CONCAT(d.nombre, ' ', d.apellido_paterno, ' ', d.apellido_materno) AS docente
FROM canalizacion c 
INNER JOIN motivos m ON c.motivo_id = m.id
INNER JOIN areas a ON c.area_id = a.id
INNER JOIN grupos g ON c.grupo_id = g.id
INNER JOIN carreras c2 ON c.carrera_id = c2.id
INNER JOIN alumnos a2 ON c.alumno_id = a2.id
INNER JOIN docentes d ON c.docente_id = d.id 
WHERE c.docente_id = {$user["id"]} AND c.tipo_solicitud='SOLICITUD'
AND c.no_caso={$_REQUEST['no_caso']}";
$requests = find_by_sql($sql, true);

//Matricula
$matricula = find_by_sql("SELECT * FROM expedientes WHERE alumno_id =" . $requests[0]["alumno_id"] . " LIMIT 1", true);

function searchForId($id, $array) {
    foreach ($array as $key => $item) {
        if ($item['motivo_id'] == $id) {
            echo " X ";
            break;
        }
    }
 }

?>
<!DOCTYPE html>
<html>

<head>
    <meta http-equiv="CONTENT-TYPE" content="text/html; charset=us-ascii">
    <title>Solicitud</title>
    <style type="text/css">
        .tg {
            border-collapse: collapse;
            border-spacing: 0;
        }

        .tg td {
            border-color: black;
            border-style: solid;
            border-width: 1px;
            font-family: Arial, sans-serif !important;
            font-size: 9px !important;
            overflow: hidden;
            padding: 10px 5px;
            word-break: normal;
        }

        .tg th {
            border-color: black;
            border-style: solid;
            border-width: 1px;
            font-family: Arial, sans-serif !important;
            font-size: 9px !important;
            font-weight: normal;
            overflow: hidden;
            padding: 10px 5px;
            word-break: normal;
        }

        .tg .tg-0lax {
            text-align: left;
            vertical-align: top
        }
    </style>
</head>

<body>
    <div type="HEADER" style="position: fixed; top: -70px!important; font-family: Arial, sans-serif !important;">
        <div type="HEADER">
            <table>
                <tr>
                    <td><img src="assets/img/fondo.png" align="CENTER" width="700px" border="0"></td>
                </tr>
                <tr>
                    <td>
                        <center><b>DEPARTAMENTO DE ASESORÍAS Y TUTORÍAS SOLICITUD DE CANALIZACIÓN</b></center>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <p align="JUSTIFY" style="margin-bottom: 0in; page-break-inside: avoid">
        <br><br><br><br><br><br>
    </p>
    <center>
        <table class="tg" style="width: 700px!important;">
            <tbody>
                <tr>
                    <td class="tg-0lax"><strong>Fecha de Solicitud:</strong> <br><?= $requests[0]['fecha'] ?></td>
                    <td class="tg-0lax"><strong>Número de caso:</strong> <?= $requests[0]['no_caso'] ?></td>
                    <td class="tg-0lax" colspan="2"></td>
                </tr>
                <tr>
                    <td class="tg-0lax" colspan="4"><strong>Nombre del alumno(a):</strong> <?= $requests[0]['alumno'] ?></td>
                </tr>
                <tr>
                    <td class="tg-0lax"><strong>Matricula:</strong> <br><?= $matricula[0]["matricula"] ?></td>
                    <td class="tg-0lax"><strong>Programa Educativo:</strong> <br><?= $requests[0]['carrera'] ?></td>
                    <td class="tg-0lax"><strong>Cuatrimestre:</strong> <br><?= $requests[0]['cuatrimestre'] ?> </td>
                    <td class="tg-0lax"><strong>Grupo:</strong> <br><?= $requests[0]['grupo'] ?></td>
                </tr>
            </tbody>
        </table>
    </center>
    <center>
        <table class="tg" style="width: 739px!important;">
            <tbody>
                <tr>
                    <td class="tg-0lax" colspan="4" style="background:#D9D9D9;text-align: center;">Motivos de la solicitud</td>
                </tr>
                <tr>
                    <td class="tg-0lax" style="width: 10px!important; text-align: center!important;"><?=searchForId(4, $requests)?></td>
                    <td class="tg-0lax" style="width: 90px!important;"><strong>Ausentismo</strong></td>
                    <td class="tg-0lax" style="width: 10px!important; text-align: center!important;"><?=searchForId(12, $requests)?></td>
                    <td class="tg-0lax" style="width: 90px!important;"><strong>Vulnerabilidad socio-económica (becas).</strong></td>
                </tr>
                <tr>
                    <td class="tg-0lax" style="width: 10px!important; text-align: center!important;"><?=searchForId(5, $requests)?></td>
                    <td class="tg-0lax" style="width: 90px!important;"><strong>Conversación soez/ actitudes agresivas (físicas o verbales).</strong></td>
                    <td class="tg-0lax" style="width: 10px!important; text-align: center!important;"><?=searchForId(13, $requests)?></td>
                    <td class="tg-0lax" style="width: 90px!important;"><strong>Influencia negativa sobre los demás.</strong></td>
                </tr>
                <tr>
                    <td class="tg-0lax" style="width: 10px!important; text-align: center!important;"><?=searchForId(6, $requests)?></td>
                    <td class="tg-0lax" style="width: 90px!important;"><strong>Apoyo jurídico</strong></td>
                    <td class="tg-0lax" style="width: 10px!important; text-align: center!important;"><?=searchForId(14, $requests)?></td>
                    <td class="tg-0lax" style="width: 90px!important;"><strong>Orientación vocacional estrategias de aprendizaje.</strong></td>
                </tr>
                <tr>
                    <td class="tg-0lax" style="width: 10px!important; text-align: center!important;"><?=searchForId(7, $requests)?></td>
                    <td class="tg-0lax" style="width: 90px!important;"><strong>Falta de respeto al maestro/ problemas con el maestro.</strong></td>
                    <td class="tg-0lax" style="width: 10px!important; text-align: center!important;"><?=searchForId(15, $requests)?></td>
                    <td class="tg-0lax" style="width: 90px!important;"><strong>Seguimiento de trayectoria educativa.</strong></td>
                </tr>
                <tr>
                    <td class="tg-0lax" style="width: 10px!important; text-align: center!important;"><?=searchForId(8, $requests)?></td>
                    <td class="tg-0lax" style="width: 90px!important;"><strong>Sospecha de consumo de enervantes.</strong></td>
                    <td class="tg-0lax" style="width: 10px!important; text-align: center!important;"><?=searchForId(16, $requests)?></td>
                    <td class="tg-0lax" style="width: 90px!important;"><strong>Problemas de salud/ educación sexual</strong></td>
                </tr>
                <tr>
                    <td class="tg-0lax" style="width: 10px!important; text-align: center!important;"><?=searchForId(9, $requests)?></td>
                    <td class="tg-0lax" style="width: 90px!important;"><strong>Signos de tristeza y/o depresión/ aislamiento.</strong></td>
                    <td class="tg-0lax" style="width: 10px!important; text-align: center!important;"><?=searchForId(17, $requests)?></td>
                    <td class="tg-0lax" style="width: 90px!important;"><strong>Otro (Especifique):</strong></td>
                </tr>
                <tr>
                    <td class="tg-0lax" style="width: 10px!important; text-align: center!important;"><?=searchForId(10, $requests)?></td>
                    <td class="tg-0lax" style="width: 90px!important;"><strong>Problemas familiares/ de pareja/ con amigos.</strong></td>
                    <td class="tg-0lax" style="width: 90px!important;" colspan="2" rowspan="2"><strong>Comentarios del solicitante</strong><br><?= $requests[0]['comentarios'] ?></td>
                </tr>
                <tr>
                    <td class="tg-0lax" style="width: 10px!important; text-align: center!important;"><?=searchForId(11, $requests)?></td>
                    <td class="tg-0lax" style="width: 90px!important;"><strong>Atención: Capacidades diferentes</strong></td>
                </tr>
            </tbody>
        </table>
    </center>
    <center>
        <table class="tg" style="width: 700px!important;">
            <tbody>
                <tr>
                    <td class="tg-0lax" colspan="3" style="background:#D9D9D9;text-align: center;"><strong>Área a canalizar</strong></td>
                </tr>
                <tr>
                    <td class="tg-0lax"><strong>Becas ( <?=($requests[0]['area_id'] == 1) ? " X " : ""?>  )</strong></td>
                    <td class="tg-0lax"><strong>Trabajo Social ( <?=($requests[0]['area_id'] == 2) ? " X " : ""?> )</strong></td>
                    <td class="tg-0lax"><strong>Medico ( <?=($requests[0]['area_id'] == 3) ? " X " : ""?> )</strong></td>
                </tr>
                <tr>
                    <td class="tg-0lax"><strong>Juridico ( <?=($requests[0]['area_id'] == 4) ? " X " : ""?> )</strong></td>
                    <td class="tg-0lax"><strong>Psicologia ( <?=($requests[0]['area_id'] == 5) ? " X " : ""?> )</strong></td>
                    <td class="tg-0lax"><strong>Direccion programa educativo ( <?=($requests[0]['area_id'] == 6) ? " X " : ""?> )</strong></td>
                </tr>
            </tbody>
        </table>
    </center>
    <p align="JUSTIFY" style="margin-bottom: 0in; page-break-inside: avoid"></p>
    <center>
        <table style="width: 700px!important;">
            <tr>
                <td>
                    <p style="text-align: center;">______________________________<br>
                        <strong style="font-family: Arial, sans-serif !important;font-size: 9px !important;">Nombre de quien solicita</strong>
                    </p>
                </td>
                <td>
                    <p style="text-align: center;">______________________________<br>
                        <strong style="font-family: Arial, sans-serif !important;font-size: 9px !important;">Nombre del responsable del área</strong>
                    </p>
                </td>
            </tr>
        </table>
    </center>
    <center>
        <table class="tg" style="width: 700px!important;">
            <tbody>
                <tr>
                    <td class="tg-0lax" colspan="3" style="background:#D9D9D9;text-align: center;"><strong>Informe del área que atendio</strong></td>
                </tr>
                <tr>
                    <td class="tg-0lax"><strong>Fecha y hora de atención</strong><br><br><br><br><br></td>
                    <td class="tg-0lax"><strong>Informe de atención</strong><br><br><br><br><br></td>
                    <td class="tg-0lax"><strong>Recomendaciones</strong><br><br><br><br><br></td>
                </tr>
            </tbody>
        </table>
    </center>
    <p align="JUSTIFY" style="margin-bottom: 0in; page-break-inside: avoid"></p>
    <center>
        <table style="width: 700px!important;">
            <tbody>
                <tr>
                    <td>
                        <p style="text-align: center;">______________________________<br>
                            <strong style="font-family: Arial, sans-serif !important;font-size: 9px !important;">Firma de recibido de quién solicitó</strong>
                        </p>
                    </td>
                    <td></td>
                    <td>
                        <p style="text-align: center;">______________________________<br>
                            <strong style="font-family: Arial, sans-serif !important;font-size: 9px !important;">Firma del área que atendio</strong>
                        </p>
                    </td>
                </tr>
            </tbody>
        </table>
    </center>
</body>

</html>