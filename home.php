<?php
$page_title = 'Dashboard';
require_once('includes/load.php');
if (!$session->isUserLoggedIn(true)) {
    redirect('index.php', false);
}

?>
<?php include_once('template/header.php'); ?>
<section id="section-container" class="section-container">
    <article class="article-container">
        <!------------------------ SECCION DE DOCENTE ------------------------------->
        <div class="article-container-contenido">
            <h2 class="title-apartado">Docente</h2>
            <form action="" class="container-formulario">
                <ul class="elements-form">
                    <li class="list-elements-form item-programa">
                        <label for="programa">Programa Educativo</label>
                        <input type="text" class="programa" id="programa" placeholder="Ingenieria Sistemas Computacionales">
                    </li>
                    <li class="list-elements-form ite-tutor">
                        <label for="tutor">Nombre del tutor</label>
                        <input type="text" class="tutor" id="tutor" placeholder="Araceli Acosta Acosta">
                    </li>
                    <li class="list-elements-form item-periodo">
                        <label for="periodo">Periodo Cuatrimestre</label>
                        <input type="text" class="periodo" id="periodo" placeholder="Septiembre-Diciembre 2020">
                    </li>
                    <ul class="sub-list">
                        <li class="list-elements-form item-grupo">
                            <label for="grupo">Grupo Tutorado</label>
                            <input type="text" class="grupo" id="grupo" placeholder="4SCG1">
                        </li>
                        <li class="list-elements-form item-hombres">
                            <label for="hombres">Total de Hombres</label>
                            <input type="number" class="hombres" id="hombres" placeholder="0">
                        </li>
                        <li class="list-elements-form item-mujeres">
                            <label for="mujeres">Total de Mujeres</label>
                            <input type="number" class="mujeres" id="mujeres" placeholder="0">
                        </li>
                    </ul>
                    <li class="list-elements-form comment">
                        <textarea name="" id="" placeholder="Agregar Comentarios"></textarea>
                    </li>
                </ul>
            </form>
        </div>
        <!------------------------ SECCION DE SESION ------------------------------->
        <div class="article-container-contenido sesion">
            <h2 class="title-apartado">Sesión</h2>
            <form action="" class="container-formulario">
                <ul class="elements-form">
                    <li class="list-elements-form item-sesion">
                        <label for="sesion">Numero de Sesión</label>
                        <input type="number" class="sesion-tab" id="sesion" placeholder="001">
                    </li>
                    <li class="list-elements-form item-parcial">
                        <label for="parcial">Corte Parcial</label>
                        <input type="number" class="parcial" id="parcial" placeholder="1,2,3">
                    </li>
                    <li class="list-elements-form item-type-sesion">
                        <p class="title-sesion">Tipo de sesión</p>
                        <div class="container-menu-select">
                            <select name="menu-select" id="list-select" class="list-select">
                                <option>Seleccion la sesión</option>
                                <option value="Tutoria" class="opcion">Tutoria</option>
                                <option value="Asesoria" class="opcion">Asesoria</option>
                            </select>
                        </div>
                    </li>
                    <li class="list-elements-form item-type-modalidad">
                        <p class="title-sesion">Tipo de Modalidad</p>
                        <div class="container-menu-select">
                            <select name="menu-select" id="list-select" class="list-select">
                                <option>Seleccion la modalidad</option>
                                <option value="grupal">Grupal</option>
                                <option value="individual" class="opcion">Individual</option>
                            </select>
                        </div>
                    </li>
                    <!------------ MENU POR SELECCION POR TABS ----------------->
                    <li class="list-elements-form comment">

                    </li>
                </ul>
            </form>
        </div>
    </article>
</section>
<!-- /.content-wrapper -->
<?php include_once('template/footer.php'); ?>