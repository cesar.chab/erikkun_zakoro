<?php
ob_start();
require_once('includes/load.php');
if ($session->isUserLoggedIn(true)) {    
    redirect('solicitud_index.php', false);
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <title>Acción Tutorial</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css">
    <link rel="stylesheet" href="<?= BASE_URL ?>/assets/css/login.css">
    <script src="<?= BASE_URL ?>/assets/js/jquery-3.5.1.js"></script>
    <style>
        /* The snackbar - position it at the bottom and in the middle of the screen */
        #snackbar {
            visibility: hidden;
            /* Hidden by default. Visible on click */
            min-width: 250px;
            /* Set a default minimum width */
            margin-left: -125px;
            /* Divide value of min-width by 2 */
            background-color: #333;
            /* Black background color */
            color: #fff;
            /* White text color */
            text-align: center;
            /* Centered text */
            border-radius: 2px;
            /* Rounded borders */
            padding: 16px;
            /* Padding */
            position: fixed;
            /* Sit on top of the screen */
            z-index: 1;
            /* Add a z-index if needed */
            left: 50%;
            /* Center the snackbar */
            bottom: 30px;
            /* 30px from the bottom */
        }

        /* Show the snackbar when clicking on a button (class added with JavaScript) */
        #snackbar.show {
            visibility: visible;
            /* Show the snackbar */
            /* Add animation: Take 0.5 seconds to fade in and out the snackbar.
                However, delay the fade out process for 2.5 seconds */
            -webkit-animation: fadein 0.5s, fadeout 0.5s 2.5s;
            animation: fadein 0.5s, fadeout 0.5s 2.5s;
        }

        /* Animations to fade the snackbar in and out */
        @-webkit-keyframes fadein {
            from {
                bottom: 0;
                opacity: 0;
            }

            to {
                bottom: 30px;
                opacity: 1;
            }
        }

        @keyframes fadein {
            from {
                bottom: 0;
                opacity: 0;
            }

            to {
                bottom: 30px;
                opacity: 1;
            }
        }

        @-webkit-keyframes fadeout {
            from {
                bottom: 30px;
                opacity: 1;
            }

            to {
                bottom: 0;
                opacity: 0;
            }
        }

        @keyframes fadeout {
            from {
                bottom: 30px;
                opacity: 1;
            }

            to {
                bottom: 0;
                opacity: 0;
            }
        }
    </style>
</head>

<body>

    <main>
        <section class="section-login">
            <article class="container-banner">
                <h2 class="subtitle-banner">Hola, Docente!!</h2>
                <p class="text-info">Nuevo portal de tutorias y asesorias, para docentes.</p>
            </article>
            <article class="login">
                <div class="container-logo-site">
                    <img src="<?= BASE_URL ?>/assets/img/logo.png" alt="" class="img-logo">
                    <h1 class="title-site">UPFIM</h1>
                </div>
                <form action="auth.php" method="POST" class="form" id="formulario_login">
                    <h2 class="subtitle">Iniciar Sesión</h2>
                    <ul class="elements">
                        <li class="list-elements">
                            <label for="matricula"><i class="fas fa-user"></i>
                                <input type="text" name="matricula" id="matricula" required placeholder="Matricula/CURP">
                            </label>
                        </li>
                        <li class="list-elements">
                            <label for="contra"><i class="fas fa-user-lock"></i>
                                <input type="password" name="contra" id="contra" required placeholder="Contraseña">
                            </label>
                        </li>
                        <li class="list-elements">
                            <!-- <button class="btn-login" type="submit">Comenzar</button> -->
                            <input type="submit" class="btn-login" id="btn_ingresar" value="Ingresar">
                        </li>
                    </ul>
                </form>
            </article>
        </section>
    </main>

</body>

</html>