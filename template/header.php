<?php $user = current_user(); ?>
<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <title><?php if (!empty($page_title))
                echo remove_junk($page_title);
            elseif (!empty($user))
                echo ucfirst($user['name']);
            else echo "Acción Tutorías"; ?>
    </title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css">
    <!-- <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" /> -->
    <!-- CSS only -->
    <!-- <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet"> -->
    <!-- JavaScript Bundle with Popper -->
    <!-- <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js"></script> -->
    <link rel="stylesheet" href="<?= BASE_URL ?>/assets/css/index.css">
    <script src="<?= BASE_URL ?>/assets/js/jquery-3.5.1.js"></script>

    <style>
        /* 
            Generic Styling, for Desktops/Laptops 
        */
        table {
            width: 100%;
            border-collapse: collapse;
        }

        /* Zebra striping */
         tr:nth-of-type(odd) {
            background: #eee;
        }

        th {
            background: #333;
            color: white;
            font-weight: bold;
        }

        td,
        th {
            padding: 6px;
            border: 1px solid #ccc;
            text-align: left;
        }
    </style>
</head>

<body>


    <!-------------------- ESTILOS DEL WRAPPER --------------------->
    <div class="wrapper-nav">
        <div class="container-logo">
            <img src="<?= BASE_URL ?>/assets/img/logo.png" alt="" class="img-logo">
            <h1 class="title-site">UPFIM</h1>
        </div>
        <nav class="navegacion">
            <ul class="nav-elements">
                <a href="<?= BASE_URL ?>solicitud_index.php">
                    <li class="list-nav-elements <?=curPageURL('solicitud') ? 'active':''?>">
                        <i class="fas fa-user"></i>
                        <p class="text-link">Solicitud </p>
                    </li>
                </a>
                <a href="<?= BASE_URL ?>plan_index.php">
                    <li class="list-nav-elements <?=curPageURL('plan') ? 'active':''?>">
                        <i class="fas fa-user-graduate"></i>
                        <p class="text-link">Plan acción</p>
                    </li>
                </a>
                <a href="retencion.html">
                    <li class="list-nav-elements">
                        <i class="fas fa-user-ninja"></i>
                        <p class="text-link">Retención</p>
                    </li>
                </a>
                <a href="historial.html">
                    <li class="list-nav-elements">
                        <i class="fas fa-history"></i>
                        <p class="text-link">Historial</p>
                    </li>
                </a>
                <a href="<?= BASE_URL ?>logout.php">
                    <li class="list-nav-elements">
                        <i class="fas fa-power-off"></i>
                        <p class="text-link">Cerrar Sesión</p>
                    </li>
                </a>
            </ul>
        </nav>
    </div>
    <!-------------------- ESTILOS DEL MAIN --------------------->
    <main>
        <!-------------------- ESTILOS DEL HEADER --------------------->
        <header>
            <div class="container-header">
                <div class="container-docente">
                    <i class="fas fa-user-circle"></i>
                    <div class="info-docente">
                        <p class="name-docente"><?= $user['nombre'] ?></p>
                        <p class="status-docente">!Bienvenido Docente!</p>
                    </div>
                </div>
                <div class="container-menu">
                    <button class="btn-menu"><i class="fas fa-bars"></i></button>
                </div>
            </div>
        </header>
        <!-------------------- ESTILOS DEL CAJA --------------------->
        <div class="caja"></div>
        <!-------------------- ESTILOS DEL CONTENIDO --------------------->