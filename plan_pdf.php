<?php
require_once('includes/load.php');
if (!$session->isUserLoggedIn(true)) {
    redirect('index.php', false);
}

$user = current_user();
$docente = "{$user['nombre']} {$user['apellido_paterno']} {$user['apellido_materno']}";

//Programas / carreras
// $programas = find_by_sql("SELECT * FROM carreras", true);

$periods = find_by_sql("SELECT * FROM periodo_cuatrimestral", true);

//Consulta para obtener los motivos de tipo SOLICITUD
$sql = "SELECT * FROM motivos WHERE tipo_motivo='PLAN'";
$reason = find_by_sql($sql, true);

//Consulta para obtener las areas
$sql = "SELECT * FROM areas";
$areas = find_by_sql($sql, true);

//Obtener alumnos
$alumnos = find_by_sql("SELECT * FROM alumnos", true);

//Obtener los tipos de sesiones
$sessions = find_by_sql("SELECT * FROM tipo_sesion", true);

//Consulta la solicitud por no_caso
$sql = "SELECT c.*, m.motivo, a.area, CONCAT(g.nomenclatura, ' ', g.anio) AS grupo,
c2.nombre AS carrera, CONCAT(a2.nombre, ' ', a2.apellido_paterno, ' ', a2.apellido_materno) AS alumno,
CONCAT(d.nombre, ' ', d.apellido_paterno, ' ', d.apellido_materno) AS docente, pc.descripcion as periodo_cuatrimestral 
FROM canalizacion c 
INNER JOIN motivos m ON c.motivo_id = m.id
INNER JOIN areas a ON c.area_id = a.id
INNER JOIN grupos g ON c.grupo_id = g.id
INNER JOIN carreras c2 ON c.carrera_id = c2.id
INNER JOIN alumnos a2 ON c.alumno_id = a2.id
INNER JOIN docentes d ON c.docente_id = d.id 
LEFT JOIN periodo_cuatrimestral pc on c.periodo_catrimestral = pc.id WHERE c.tipo_solicitud='PLAN' AND c.no_caso=" . $_REQUEST["no_caso"];
$requests = find_by_sql($sql, true);

//Obtener el docente
$docente_solicitud = find_by_id("docentes", $requests[0]['docente_id'], "id");

//Matricula
$matricula = find_by_sql("SELECT * FROM expedientes WHERE alumno_id =" . $requests[0]["alumno_id"] . " LIMIT 1", true);

$grupos = find_by_sql("SELECT * FROM grupos WHERE id = {$requests[0]["grupo_id"]} LIMIT 1", true);
/*carrera_id={$requests[0]['carrera_id']} AND periodo_cuatrimestral_id={$requests[0]['periodo_catrimestral']}", true*/

$sessionSelected = find_by_sql("SELECT s.* FROM sesion s 
INNER JOIN canalizacion c ON s.no_sesion = c.no_caso AND c.tipo_solicitud ='PLAN' 
INNER JOIN tipo_sesion ts ON s.tipo_sesion_id = ts.id 
WHERE no_sesion = {$requests[0]['no_caso']} LIMIT 1", true);


?>
<!DOCTYPE html>
<html>

<head>
    <meta http-equiv="CONTENT-TYPE" content="text/html; charset=us-ascii">
    <title>Solicitud</title>
    <style type="text/css">
        .tg {
            border-collapse: collapse;
            border-spacing: 0;
        }

        .tg td {
            border-color: black;
            border-style: solid;
            border-width: 1px;
            font-family: Arial, sans-serif !important;
            font-size: 9px !important;
            overflow: hidden;
            padding: 10px 5px;
            word-break: normal;
        }

        .tg th {
            border-color: black;
            border-style: solid;
            border-width: 1px;
            font-family: Arial, sans-serif !important;
            font-size: 9px !important;
            font-weight: normal;
            overflow: hidden;
            padding: 10px 5px;
            word-break: normal;
        }

        .tg .tg-0lax {
            text-align: left;
            vertical-align: top
        }
    </style>
</head>

<body>
    <div type="HEADER" style="position: fixed; top: -70px!important; font-family: Arial, sans-serif !important;">
        <div type="HEADER">
            <table>
                <tr>
                    <td><img src="assets/img/fondo.png" align="CENTER" width="700px" border="0"></td>
                </tr>
                <tr>
                    <td>
                        <center><b>PROGRAMA CUATRIMESTRAL DE ACCIÓN TUTORIAL</b></center>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <p align="JUSTIFY" style="margin-bottom: 0in; page-break-inside: avoid">
        <br><br><br><br><br><br>
    </p>
    <center>
        <table class="tg" style="width: 700px!important;">
            <tbody>
                <tr>
                    <td class="tg-0lax"><strong>Fecha registro:</strong> <br><?= $requests[0]['fecha'] ?></td>
                    <td class="tg-0lax"><strong>Programa Educativo:</strong> <br><?= $requests[0]['carrera'] ?></td>
                    <td class="tg-0lax"><strong>Cuatrimestre:</strong> <br><?= $requests[0]['cuatrimestre'] ?> </td>
                </tr>
                <tr>
                    <td class="tg-0lax"><strong>Período Cuatrimestral:</strong> <br><?= $requests[0]["periodo_cuatrimestral"] ?></td>                   
                    <td class="tg-0lax"><strong>Nombre del docente:</strong> <br><?= $requests[0]['docente'] ?></td>
                    <td class="tg-0lax"><strong>Jefe del grupo:</strong> <br><?= $requests[0]['alumno'] ?></td>
                </tr>
                <tr>
                    <td class="tg-0lax"><strong>Matricula:</strong> <br><?= $matricula[0]["matricula"] ?></td>                   
                    <td class="tg-0lax"><strong>Grupo:</strong> <br><?= $requests[0]['grupo'] ?></td>
                    <td class="tg-0lax"><strong>Corte Parcial:</strong> <br> <?=$sessionSelected[0]["parcial"]?></td>
                </tr>
                <tr>
                    <td class="tg-0lax"><strong>Total Mujeres:</strong> <br><?= $grupos[0]["total_mujeres"] ?></td>                   
                    <td class="tg-0lax"><strong>Total Hombres:</strong> <br><?= $grupos[0]['total_hombres'] ?></td>
                    <td class="tg-0lax"><br></td>
                </tr>
            </tbody>
        </table>
    </center>
    <center>
        <table class="tg" style="width: 700px!important;">
            <tbody>
                <tr>
                    <td class="tg-0lax" colspan="2" style="background:#D9D9D9;text-align: center;">Tipo de canalización</td>
                </tr>
                <tr>
                    <td class="tg-0lax" style="text-align: center!important;"><strong>Asesoría ( <?=($sessionSelected[0]["tipo_sesion_id"]== 1) ? " X " : "" ?> ) </strong> </td>                    
                    <td class="tg-0lax" style="text-align: center!important;"><strong>Tutoría ( <?=($sessionSelected[0]["tipo_sesion_id"]== 2) ? " X " : "" ?> )</strong></td>
                </tr>
            </tbody>
        </table>
    </center>
    <center>
        <table class="tg" style="width: 700px!important;">
            <tbody>
                <tr>
                    <td class="tg-0lax" colspan="3" style="background:#D9D9D9;text-align: center;"><strong>Área a canalizar</strong></td>
                </tr>
                <tr>
                    <td class="tg-0lax"><strong>Becas ( <?=($requests[0]['area_id'] == 1) ? " X " : ""?>  )</strong></td>
                    <td class="tg-0lax"><strong>Trabajo Social ( <?=($requests[0]['area_id'] == 2) ? " X " : ""?> )</strong></td>
                    <td class="tg-0lax"><strong>Medico ( <?=($requests[0]['area_id'] == 3) ? " X " : ""?> )</strong></td>
                </tr>
                <tr>
                    <td class="tg-0lax"><strong>Juridico ( <?=($requests[0]['area_id'] == 4) ? " X " : ""?> )</strong></td>
                    <td class="tg-0lax"><strong>Psicologia ( <?=($requests[0]['area_id'] == 5) ? " X " : ""?> )</strong></td>
                    <td class="tg-0lax"><strong>Direccion programa educativo ( <?=($requests[0]['area_id'] == 6) ? " X " : ""?> )</strong></td>
                </tr>
            </tbody>
        </table>
    </center>
    <center>
        <table class="tg" style="width: 700px!important;">
            <tbody>
                <tr>
                    <td class="tg-0lax" colspan="3" style="background:#D9D9D9;text-align: center;"><strong>Informe del área que atendio</strong></td>
                </tr>
                <tr>
                    <td class="tg-0lax" colspan="3"><strong>Comentarios del tutor:</strong><br> <?=$sessionSelected[0]["comentarios"]?></td>                    
                </tr>
                <tr>
                    <td class="tg-0lax" colspan="3"><strong>Temática a abordar:</strong><br> <?=$sessionSelected[0]["tematica"]?></td>                    
                </tr>
                <tr>
                    <td class="tg-0lax" colspan="3"><strong>Resultados esperados:</strong><br> <?=$sessionSelected[0]["resultados_esperados"]?></td>                    
                </tr>
                <tr>
                    <td class="tg-0lax" colspan="3"><strong>Resultados obtenidos:</strong><br> <?=$sessionSelected[0]["resultados_obtenidos"]?></td>                    
                </tr>
            </tbody>
        </table>
    </center>

    <p align="JUSTIFY" style="margin-bottom: 0in; page-break-inside: avoid"></p>
    <center>
        <table style="width: 700px!important;">
            <tr>
                <td>
                    <p style="text-align: center;">____________________<br>
                        <strong style="font-family: Arial, sans-serif !important;font-size: 9px !important;">Nombre y firma <br> Director de programa educativo</strong>
                    </p>
                </td>
                <td>
                    <p style="text-align: center;">____________________<br>
                        <strong style="font-family: Arial, sans-serif !important;font-size: 9px !important;">Nombre y firma <br>del tutor</strong>
                    </p>
                </td>
                <td>
                    <p style="text-align: center;">____________________<br>
                        <strong style="font-family: Arial, sans-serif !important;font-size: 9px !important;">Nombre y firma del responsable <br> departamento de asesorias y tutorias</strong>
                    </p>
                </td>
            </tr>
        </table>
    </center>    
</body>

</html>