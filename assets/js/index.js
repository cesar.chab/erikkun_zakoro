$(document).ready(function(){
   $('.tabs li a:first').addClass('active'); 
    $('.secciones-tab .article-tab').hide();
    $('.secciones-tab .article-tab:first').show();
    
    $('.tabs li a').click(function(){
       $('.tabs li a').removeClass('active');
        $(this).addClass('active');
        $('.secciones-tab .article-tab').hide();
        
        
        var mostrar = $(this).attr('href');
        $(mostrar).show();
        return false;
    });
});

$('.btn-menu').click(function(){
    $('.wrapper-nav').toggleClass('menu');
});