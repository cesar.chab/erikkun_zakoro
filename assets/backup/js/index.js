//CODIGO DE JS DE NAVEGACION TABS
$(document).ready(function(){
    $('ul.tabs li a:first').addClass('active');
    $('.secciones-wrap article').hide();
    $('.secciones-wrap article:first').show();
    
    $('ul.tabs li a').click(function(){
       $('ul.tabs li a').removeClass('active');
        $(this).addClass('active');
        $('.secciones-wrap article').hide();
        
        var mostrar = $(this).attr('href');
        $(mostrar).show();
        return false;
    });
    
});
