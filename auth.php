<?php include_once('includes/load.php'); ?>
<?php
$req_fields = array('matricula', 'contra');
validate_fields($req_fields);
$username = remove_junk($_POST['matricula']);
$password = remove_junk($_POST['contra']);

if (empty($errors)) {
  $user_id = authenticate($username, $password);


  if ($user_id) {
    //create session with id
    $session->login($user_id);
    //Update Sign in time
    $session->msg("s", "Bienvenido a Acción Tutorias.");
    redirect('solicitud_index.php', false);
  } else {
    $session->msg("d", "Matricula y/o contraseña incorrecto.");
    redirect('index.php', false);
  }
} else {
  $session->msg("d", $errors);
  redirect('index.php', false);
}

?>
